package ru.itis.javalab.repositories;

import ru.itis.javalab.models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SimpleJdbcTemplate<T> {
    private DataSource dataSource;

    public SimpleJdbcTemplate(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> List<T> query(String sql, RowMapper<T> rowMapper, Object ... args) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql+ args[0]);
            resultSet = statement.executeQuery();

            List<T> users = new ArrayList<>();

            while (resultSet.next()) {
                T user = rowMapper.mapRow(resultSet);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignore) {
                }
            }
            if (statement != null){
                try {
                    connection.close();
                } catch (SQLException ignore){}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignore){}
            }

        }
    }
    public <T> Optional<T> findingDeletingByArgs(String sql, RowMapper<T> rowMapper, Object ... args){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        T user=null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);

            for (int i=0;i<args.length;i++){
                statement.setObject(i+1,args[i]);
            }
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = rowMapper.mapRow(resultSet);
            }
            return  Optional.ofNullable(user);

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignore) {
                }
            }
            if (statement != null){
                try {
                    connection.close();
                } catch (SQLException ignore){}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignore){}
            }

        }
    }
    public <T> void saveDeleteUpdateByEntity(String sql, Object ... args){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        T user=null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            //найти через рефлексию поля.
            // Добавить в бд


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignore) {
                }
            }
            if (statement != null){
                try {
                    connection.close();
                } catch (SQLException ignore){}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignore){}
            }

        }
        
    }







}
