package ru.itis.javalab.repositories;

import ru.itis.javalab.models.Product;

public interface ProductsRepository extends CrudRepository<Product> {


}
