package ru.itis.javalab.app;

import com.beust.jcommander.IStringConverter;

public class UrlConverter implements IStringConverter<String[]> {
    @Override
    public String[] convert(String s) {
        return s.split(";");
    }
}