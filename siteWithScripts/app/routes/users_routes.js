module.exports = function (app) {
    app.get('/data', (request, response) => {
        var result = [{
            "id": 1,
            "data": "Name: Azat"
        },
            {
                "id": 2,
                "data": "Age: 19"
            },
            {
                "id": 3,
                "data": "Email: azat.nabiev-azat@yandex.ru"
            },
            {
                "id":4,
                "data": "Numb: 82342342322"
            },
            {
                "id": 5,
                "data": "Exp: 0"
            },
            {
                "id": 6,
                "data": "Gender: male"
            },
            {
                "id": 7,
                "data": "Languages:  java"
            }
        ];
        response.send(JSON.stringify(result));
    });

};
