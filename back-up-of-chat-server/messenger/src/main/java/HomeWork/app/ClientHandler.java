package HomeWork.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private Server server;
    private Socket client;
    BufferedReader fromClient;
    PrintWriter toClient;

    public ClientHandler(Socket socket, Server server){
        this.server=server;
        this.client=socket;
        try {
            fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
            toClient  = new PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("thread created");
        while (true) {

            try {
                String message = fromClient.readLine();
                System.out.print("result:");
                System.out.println(message);
                if (message != null) {
                    server.sendMessageToAllClients(message);
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void sendMsg(String msg){
        try {
            toClient.println(msg);
            toClient.flush();
        }catch (Exception ex){
            throw new IllegalStateException(ex);
        }
    }

}
