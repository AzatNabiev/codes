package HomeWork.app;

import HomeWork.utils.Args;
import com.beust.jcommander.JCommander;

public class ServerStarter {
    public static void main(String[] argv) {
        Args args= new Args();

        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);

        Integer port=args.port;
        Server server=new Server();
        server.start(7777);
    }
}
