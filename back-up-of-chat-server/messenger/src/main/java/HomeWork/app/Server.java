package HomeWork.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
        private List<ClientHandler> clients= new ArrayList<>();

    public void start(int port) {
        ServerSocket server;

        try {
            server = new ServerSocket(port);
            System.out.println("server started");
            while (true) {
                Socket client = server.accept();
                System.out.println("client connected");

               ClientHandler clientHandler = new ClientHandler(client, this);
               clients.add(clientHandler);
               new Thread(clientHandler).start();
                System.out.println("thread started");
            }

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
    public void sendMessageToAllClients(String msg){
        for (ClientHandler client: clients){
            client.sendMsg(msg);
        }
    }
}
