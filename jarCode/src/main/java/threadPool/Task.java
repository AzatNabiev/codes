package threadPool;

import app.ImageDownLoader;

public class Task extends ImageDownLoader implements Runnable {

    private String url;
    private String pathToSave;

    public Task(String url, String pathToSave) {
        this.url=url;
        this.pathToSave=pathToSave;
    }

    public void run() {
        download(url,pathToSave);
    }
}
