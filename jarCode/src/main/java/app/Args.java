package app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators="=")
public class Args {
    @Parameter(names={"--mode"})
    public String mode;

    @Parameter(names={"--files"}, converter = UrlConverter.class)
    public String[] urls;

    @Parameter(names={"--folder"})
    public String folderUrl;

    @Parameter(names={"--count"})
    public Integer count;

}
