package app;

import com.beust.jcommander.IStringConverter;

import java.util.List;

public class UrlConverter implements IStringConverter<String[]> {
    @Override
    public String[] convert(String s) {
        return s.split(";");
    }
}
