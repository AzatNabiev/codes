package app;

import com.beust.jcommander.JCommander;
import threadPool.Task;
import threadPool.ThreadPool;

public class Program {

    private static final String ONE_THREAD="one-thread";
    private static final String MULTI_THREAD="multi-thread";

    public static void main(String[] argv) {
        Args args = new Args();

        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);
        ImageDownLoader imageDownLoader = new ImageDownLoader();

        String mode = args.mode;
        String folderUrl = args.folderUrl;
        String[] urls = args.urls;

        if (mode != null && folderUrl != null && urls != null) {
            if (mode.equals(ONE_THREAD)) {
                for (String url : urls)
                    imageDownLoader.download(url, folderUrl);
            } else if (mode.equals(MULTI_THREAD)) {
                ThreadPool threadPool=new ThreadPool(args.count);
                Task task;
                for (String url: urls){
                    task=new Task(url, folderUrl);
                    threadPool.submit(task);
                }
            }
        }

    }
}
