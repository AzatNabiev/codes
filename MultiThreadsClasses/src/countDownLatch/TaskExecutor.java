package countDownLatch;

public interface TaskExecutor {
    void submit(Runnable task);
}
