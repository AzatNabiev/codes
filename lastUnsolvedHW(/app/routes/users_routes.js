bodyParser=require('body-parser').json();
const fs = require('fs');
module.exports = function (app) {
    app.get('/data', (request, response) => {
        const {Client} = require('pg');
        const client = new Client({
            user: 'postgres',
            database: 'postgres',
            host: 'localhost',
            password: 'qwerty007'
        });
        let query = 'select * from postgres.public.data';

        client.connect();
        client.query(query, function (error, result) {
            if (error != null) {
                response.send("Incorrect data! Try again.")
            } else response.send(JSON.stringify(result.rows));

            client.end();
        });
    });

    app.post('/data', bodyParser, function (request, response) {
        const {Client} = require('pg');
        const client = new Client({
            user: 'postgres',
            database: 'postgres',
            host: 'localhost',
            password: "qwerty007"
        });
        client.connect();

        let email = `${request.body.email}`;
        let query = 'insert into postgres.public.data (email) value $1';
        let ok = '<br><a href="/index.html"><button>OK</button></a>';

        client.query(query, email, function (err) {
            if (err != null) {
                response.send("incorrect data." + ok);
            }

            else {
                response.send("Спасибо за вашу заявку!" + ok);
            }
            client.end();
        });
    });

};
