package ru.itis.repositories;

import org.w3c.dom.ls.LSOutput;
import ru.itis.models.Mentor;
import ru.itis.models.Student;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class StudentsRepositoryJdbcImpl implements StudentsRepository {
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id =";
    //language=SQL
    private static final String SQL_SAVE_OBJECT = "insert into student(first_name, last_name, age, group_number) VALUES (?,?,?,?)";
    //language=SQL
    private static final String SQL_MENTORS_INSERT = "insert into mentor(first_name, last_name,student_id) VALUES (?,?,?)";
    //language=SQL
    private static final String SQL_SELECT_MENTORS_ID = "select * from mentor where mentor.student_id= ";
    //language=SQL
    private static final String SQL_UPDATE_STUDENTS_ID = "update student set first_name=?, last_name=?, age=?, group_number=? where id=";
    //language=SQL
    private static final String SQL_DELETE_MENTOR_ID = "delete from mentor where id=?";
    //language=SQL
    private static final String SQL_SELECT_STUDENT_MENTOR = "select st.id as s_id, st.first_name as s_name, st.last_name as s_lname, age, group_number, m.id as m_id,\n" +
            "       m.first_name as m_name, m.last_name as m_lname, subject_id, student_id\n" +
            "from student st left\n" +
            "    join mentor m on st.id=m.student_id ";

    private Connection connection;
    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> findAllByAge(int age) {

        Statement statement = null;
        ResultSet result = null;
        Student student = null;
        List<Student> students = new ArrayList<>();
       // List<Mentor> mentors = new ArrayList<>();
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_STUDENT_MENTOR + " where age =" + age + "order by s_id");
            long temp = 0;
            while (result.next()) {
                long actualId = result.getLong("s_id");
                if (temp != actualId && actualId > 0) {
                    temp = actualId;
                    student = new Student(result.getLong("s_id"),
                            result.getString("s_name"),
                            result.getString("s_lname"),
                            result.getInt("age"),
                            result.getInt("group_number"));
                    students.add(student);
                }
                 if (result.getLong("m_id")>0) {
                     student.addMentor(new Mentor(result.getLong("m_id"),
                             result.getString("m_name"),
                             result.getString("m_lname"),
                             student));
                 }
            }
            return students;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public List<Student> findAll() {
        Statement statement = null;
        ResultSet result = null;
        Student student = null;
        List<Student> students = new ArrayList<>();
       // List<Mentor> mentors = new ArrayList<>();
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_STUDENT_MENTOR + "order by s_id");
            long temp = 0;
            while (result.next()) {
                long actualId = result.getLong("s_id");
                if (temp != actualId && actualId > 0) {
                    temp = actualId;
                    student = new Student(result.getLong("s_id"),
                            result.getString("s_name"),
                            result.getString("s_lname"),
                            result.getInt("age"),
                            result.getInt("group_number"));
                    students.add(student);
                }
                    student.addMentor(new Mentor(result.getLong("m_id"),
                            result.getString("m_name"),
                            result.getString("m_lname"),
                            student));
            }
            return students;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }

    }

    @Override
    public Student findById(Long id) {

        Statement statement = null;
        ResultSet resultS = null;

       // List<Mentor> mentors = new ArrayList<>();

        Student student = null;

        try {
            statement = connection.createStatement();
            resultS = statement.executeQuery(SQL_SELECT_BY_ID + id);

            if (resultS.next()) {
                student = new Student(resultS.getLong("id"),
                        resultS.getString("first_name"),
                        resultS.getString("last_name"),
                        resultS.getInt("age"),
                        resultS.getInt("group_number"));


                resultS = statement.executeQuery(SQL_SELECT_MENTORS_ID + resultS.getLong("id"));
                while (resultS.next()) {

                    student.addMentor(new Mentor(resultS.getLong("id"),
                            resultS.getString("first_name"),
                            resultS.getString("last_name"),
                            student));
                }
            }
            return student;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultS != null) {
                try {
                    resultS.close();
                } catch (SQLException e) {
                    // ignore
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public void save(Student entity) {
        PreparedStatement state = null;
        long s_id = 0, m_id = 0;
        try {
            state = connection.prepareStatement(SQL_SAVE_OBJECT, Statement.RETURN_GENERATED_KEYS);

            state.setString(1, entity.getFirstName());
            state.setString(2, entity.getLastName());
            state.setInt(3, entity.getAge());
            state.setInt(4, entity.getGroupNumber());

            long affectedRows = state.executeUpdate();

            if (affectedRows > 0) {
                try {
                    ResultSet rs = state.getGeneratedKeys();
                    if (rs.next()) {
                        s_id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    //ignore
                }
            }
            entity.setId(s_id);
            if (entity.getMentorsList() != null) {
                try {
                    state = connection.prepareStatement(SQL_MENTORS_INSERT, Statement.RETURN_GENERATED_KEYS);
                    List<Mentor> mentors = entity.getMentorsList();
                    for (Mentor mentor : mentors) {
                        state.setString(1, mentor.getFirstName());
                        state.setString(2, mentor.getLastName());
                        state.setLong(3, s_id);

                        long rows = state.executeUpdate();
                        if (rows > 0) {
                            try {
                                ResultSet rs = state.getGeneratedKeys();
                                if (rs.next()) {
                                    m_id = rs.getLong(1);
                                }
                            } catch (SQLException ex) {
                                //ignore
                            }
                        }
                        mentor.setId(m_id);
                    }
                } catch (SQLException e) {
                    //ignore
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (state != null) {
                try {
                    state.close();
                } catch (SQLException e) {
                    //ignore
                }
            }
        }
    }

    @Override
    public void update(Student entity) {
        List<Mentor> deletedMentors;
        List<Mentor> newMentors;
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(SQL_UPDATE_STUDENTS_ID + entity.getId(), Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setLong(3, entity.getAge());
            statement.setLong(4, entity.getGroupNumber());

            statement.executeUpdate();

            deletedMentors = entity.getDeletedMentors();
            System.out.println(deletedMentors.toString());

            if (!deletedMentors.isEmpty()) {
                statement = connection.prepareStatement(SQL_DELETE_MENTOR_ID, Statement.RETURN_GENERATED_KEYS);
                for (Mentor mentor : deletedMentors) {

                    statement.setLong(1, mentor.getId());
                    statement.executeUpdate();
                }
                deletedMentors.clear();
            }

            newMentors = entity.getNewMentors();
            if (!newMentors.isEmpty()) {
                statement = connection.prepareStatement(SQL_MENTORS_INSERT, Statement.RETURN_GENERATED_KEYS);
                for (Mentor mentor : newMentors) {
                    statement.setString(1, mentor.getFirstName());
                    statement.setString(2, mentor.getLastName());
                    statement.setLong(3, entity.getId());
                    statement.executeUpdate();
                }
                newMentors.clear();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }
}

