package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Mentor;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarOutputStream;

public class Main {

    private static final String URL="jdbc:postgresql://localhost:5432/firstLesson";
    private static final String USER="postgres";
    private static final String PASSWORD="qwerty007";

    public static void main(String[] args) throws SQLException {
        Connection connection= DriverManager.getConnection(URL,USER,PASSWORD);
        StudentsRepository studentsRepository=new StudentsRepositoryJdbcImpl(connection);

        List<Student> students=studentsRepository.findAll();
        List<Mentor> mentors=students.get(0).getMentorsList();
        System.out.println(students);
        System.out.println(mentors);








    }
}
