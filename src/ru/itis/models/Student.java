package ru.itis.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Student {
    private Long id;
    private String firstName;
    private String lastName;
    private int age;
    private int groupNumber;
    List<Mentor> mentors ;
    List<Mentor> newMentors=new ArrayList<>();
    List<Mentor> deletedMentors=new ArrayList<>();

    public Student(Long id, String firstName, String lastName, int age, int groupNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.groupNumber = groupNumber;
    }


    public Student(Long id, String firstName, String lastName, int age, int groupNumber, List<Mentor>mentors) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.groupNumber = groupNumber;
        this.mentors=mentors;
    }

    public Student() {

    }

    public void addMentors(Mentor mentor){
        newMentors.add(mentor);
        mentors.add(mentor);
    }
    public void addMentor(Mentor mentor){
        mentors.add(mentor);
    }
    public List<Mentor> getNewMentors(){return newMentors;}

    public void setMentorsList(List<Mentor> mentors){
        this.mentors=mentors;
    }
    public void deleteMentors(int i){
        deletedMentors.add(mentors.remove(i));
    }
    public List<Mentor> getDeletedMentors(){return  deletedMentors;}
    public List<Mentor> getMentorsList(){
        return mentors;
    }
    public Mentor getMentor(int i){
        return mentors.get(i);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age &&
                groupNumber == student.groupNumber &&
                Objects.equals(id, student.id) &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(lastName, student.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, age, groupNumber);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", groupNumber=" + groupNumber +
                '}';
    }
}
