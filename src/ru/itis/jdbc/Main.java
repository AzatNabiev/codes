package ru.itis.jdbc;

import java.sql.*;

public class Main {

    private static final String URL="jdbc:postgresql://localhost:5432/firstLesson";
    private static final String USER="postgres";
    private static final String PASSWORD="qwerty007";

    public static void main(String[] args) throws SQLException {

        SimpleDataSource dataSource=new SimpleDataSource();
        Connection connection=dataSource.openConnection(URL,USER,PASSWORD);

        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery("select * from student");

        while (resultSet.next()){
            System.out.println("ID " + resultSet.getInt("id"));
            System.out.println("First Name "+ resultSet.getString("last_name"));
            System.out.println("Last Name " + resultSet.getString("first_name"));
            System.out.println("Age "+ resultSet.getInt("age"));
            System.out.println("Group Number "+ resultSet.getInt("group_number"));

        }
        resultSet.close();
        resultSet=statement.executeQuery("select * from student s full outer join mentor m on s.id=m.student_id");
        while (resultSet.next()){
            System.out.println("ID "+ resultSet.getInt("Id"));
        }

        connection.close();
    }
}
