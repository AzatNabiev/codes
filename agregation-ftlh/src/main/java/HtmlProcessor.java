import com.google.auto.service.AutoService;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@AutoService(Processor.class)
@SupportedAnnotationTypes(value =  {"HtmlForm", "HtmlInput"})
public class HtmlProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(HtmlForm.class);

        for (Element element : elements) {
            String path = HtmlProcessor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            path = path.substring(1) + element.getSimpleName().toString() + ".html";
            Path out = Paths.get(path);
            Configuration conf = new Configuration(Configuration.VERSION_2_3_30);
            conf.setDefaultEncoding("UTF-8");
            try {
                String ftlhFilePath="C:\\Users\\Acer\\Desktop\\tomcatt\\agregation\\src\\main\\resources\\ftlh";
                conf.setTemplateLoader(new FileTemplateLoader(new File(ftlhFilePath)));
                List<Map<String, String>> inputs = new ArrayList<>();
                Map<String, String> inside = new HashMap<>();
                HtmlForm annotation = element.getAnnotation(HtmlForm.class);
                for (Element value : element.getEnclosedElements()) {
                    HtmlInput htmlInput = value.getAnnotation(HtmlInput.class);
                    if (htmlInput != null) {
                        inside.put("type", htmlInput.type());
                        inside.put("name", htmlInput.name());
                        inside.put("placeholder", htmlInput.placeholder());
                        inputs.add(inside);
                    }
                }
                Map<String, Object> attribute = new HashMap<>();
                attribute.put("inputs", inputs);
                attribute.put("action", annotation.action());
                attribute.put("method", annotation.method());
                Template template = conf.getTemplate("User.ftlh");

                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(out.toFile().getAbsolutePath()));
                    template.process(attribute, writer);

                } catch (IOException | TemplateException e) {
                    throw new IllegalStateException(e);
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return true;
    }

}
