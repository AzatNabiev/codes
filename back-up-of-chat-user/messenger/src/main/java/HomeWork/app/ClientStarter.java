package HomeWork.app;

import HomeWork.utils.Args;
import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class ClientStarter {
    public static void main(String[] argv) {
        Args args= new Args();

        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);
        Integer port=args.port;
        String serverIp=args.serverIp;
        Client client=new Client(serverIp,port);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);
        }

    }
}
