package ru.itis.javalab.repositories;

import org.graalvm.compiler.nodes.calc.IntegerDivRemNode;
import ru.itis.javalab.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends CrudRepository<User> {
    List<User> findAllByAge(Integer age);

    Optional<User> findFirstByEmailAndPassword(String email, String pass);
    Optional<User> findUserByEmail(String email);
}

