package ru.itis.javalab.servlets;

import ru.itis.javalab.dto.SignUpForm;
import ru.itis.javalab.services.CookiesService;
import ru.itis.javalab.services.SignUpService;
import ru.itis.javalab.services.UsersService;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SignUpServlet extends HttpServlet {
    private CookiesService cookiesService;
    private UsersService usersService;
    private SignUpService signUpService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context=config.getServletContext();
        cookiesService=(CookiesService)context.getAttribute("cookiesService");
        usersService=(UsersService)context.getAttribute("usersService");
        signUpService=(SignUpService)context.getAttribute("signUpService");
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/public/signup.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignUpForm form = new SignUpForm();
        String password = req.getParameter("password");
        String password_repeat = req.getParameter("password_repeat");

        if (password.equals(password_repeat)) {
            form.setFirstName(req.getParameter("firstName"));
            form.setLastName(req.getParameter("lastName"));
            form.setEmail(req.getParameter("email"));
            form.setPassword(req.getParameter("password"));
            signUpService.signUp(form);
            resp.sendRedirect("public/login.html");
        } else {
            resp.sendRedirect("public/signup.html");
        }
    }
}
