package ru.itis.javalab.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.UUID;

@WebServlet("/fileUpload")
@MultipartConfig
public class UploadServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("public/fileUploadForm.html").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String basicPath="C://files";
        HttpSession session=req.getSession(false);
        String userId= session.getAttribute("userId").toString();

        Part part=req.getPart("file");

        Path path= Paths.get(basicPath+ File.separator+ userId);
        Files.createDirectories(path);

        UUID numb = UUID.randomUUID();

        Files.copy(part.getInputStream(), Paths.get(basicPath+File.separator+userId+File.separator+numb.toString()+part.getSubmittedFileName()));
        resp.sendRedirect("/fileUpload");

    }


}
