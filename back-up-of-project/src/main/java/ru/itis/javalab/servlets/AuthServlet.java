package ru.itis.javalab.servlets;

import ru.itis.javalab.models.User;
import ru.itis.javalab.services.BCrypterService;
import ru.itis.javalab.services.CookiesService;
import ru.itis.javalab.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;


public class AuthServlet extends HttpServlet {
    private CookiesService cookiesService;
    private UsersService usersService;
    private BCrypterService bCrypterService;

    @Override
    public void init(ServletConfig config) {
        ServletContext context = config.getServletContext();
        cookiesService = (CookiesService) context.getAttribute("cookiesService");
        usersService = (UsersService) context.getAttribute("usersService");
        bCrypterService=(BCrypterService) context.getAttribute("bCrypterService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("/public/login.html").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("login").trim();
        String pass = req.getParameter("password").trim();

        Optional<User> user = usersService.findUserByEmail(email);

        if (user.isPresent() && bCrypterService.checkPass(pass, user.get().getPassword())) {
            HttpSession session=req.getSession(true);
            session.setAttribute("userId", user.get().getId());
            session.setAttribute("authenticated",true);
            resp.sendRedirect("/profile" );
        } else {
            resp.sendRedirect("/login");
        }
    }
}
