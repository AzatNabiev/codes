package ru.itis.javalab.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@WebServlet("/image")
public class DownLoadServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session=req.getSession(false);
        String userId=session.getAttribute("userId").toString();
        File[] file=new File("C://files"+File.separator+userId+File.separator).listFiles();
            resp.setContentType("image/jpg");
            resp.setContentLengthLong((int)file[0].length());
            String name=file[0].getName();
            resp.setHeader("Content-Description","filename=\"name.jpg\"");
            Files.copy(file[0].toPath(), resp.getOutputStream());
            resp.flushBuffer();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
