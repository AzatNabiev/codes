package ru.itis.javalab.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UploadService {


    public static void upload(String userUrl, HttpServletRequest request) throws IOException, ServletException {
        Part part=request.getPart("file");
        Path path= Files.createDirectory(Paths.get(userUrl));
        Files.copy(part.getInputStream(), Paths.get(userUrl+ File.separator+part.getSubmittedFileName()));
    }
}
