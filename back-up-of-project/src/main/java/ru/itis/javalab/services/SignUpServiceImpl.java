package ru.itis.javalab.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.javalab.dto.SignUpForm;
import ru.itis.javalab.models.User;
import ru.itis.javalab.repositories.UsersRepository;

public class SignUpServiceImpl implements SignUpService {
    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    public SignUpServiceImpl(UsersRepository usersRepository){
        this.usersRepository=usersRepository;
        this.passwordEncoder=new BCryptPasswordEncoder();
    }

    @Override
    public void signUp(SignUpForm form) {
        User user=User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .build();
        usersRepository.save(user);

    }
}
