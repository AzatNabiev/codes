package ru.itis.javalab.services;

import javax.servlet.http.Cookie;
import java.util.Optional;

public interface CookiesService {
    void addCookieToDbByUserId(Long userId, String toString);
    Boolean CheckCookiesByID(Long id);
    Boolean CheckCookiesByValue(String value);
    Optional<Cookie> findAuthCookie(Cookie[] cookieArray);
}
