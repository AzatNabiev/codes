package threadPool;

import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        String[]names={"1","2","3","4","5","6","7"};
        threadPool.ThreadPool pool = new threadPool.ThreadPool(7,names);

        for (int i = 0; i < 7; i++) {
            threadPool.Task task = new threadPool.Task(i);
            pool.submit(task);
        }
    }
}
