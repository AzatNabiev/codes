package threadPool;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

public class ThreadPool {

    private Deque<Runnable> tasks;

    private PoolWorker[] pool;

    ThreadPool (int threadsCount, String names[]) {

        tasks = new ConcurrentLinkedDeque<>();
        pool = new PoolWorker[threadsCount];


        for (int i = 0; i < pool.length; i++) {
            pool[i]=new PoolWorker(names[i]);
            pool[i].start();
            System.out.println("начинаем");
        }
    }



    public void submit(Runnable task) {
        synchronized (tasks){
            tasks.add(task);
            tasks.notify();
        }
    }


    private class PoolWorker extends Thread {

        PoolWorker(String name){

            super(name);
        }
        public void run() {

            Runnable task;

            while (true) {
                 synchronized (tasks) {
                     while (tasks.isEmpty()) {
                         try {
                             System.out.println("thread #"+ PoolWorker.currentThread().getName()+" is waiting");
                             tasks.wait();
                         } catch (InterruptedException e) {
                             throw new IllegalArgumentException(e);
                         }
                     }
                     task = tasks.poll();
                 }
                 try {
                     task.run();
                 } catch (Exception e) {
                     throw new IllegalArgumentException(e);
                 }
            }
        }
    }
}