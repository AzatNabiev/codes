package ru.itis.javalab.repositories.old;

//import ru.itis.javalab.models.User;
//
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//public class UsersRepositoryJdbcImpl implements UsersRepository {
//
//    //language=SQL
//    private static final String SQL_SELECT_BY_AGE = "select * from student where age= ";
//
//    //language=SQL
//    private static final String SQL_INSERT_ENTITY="insert into student(first_name, last_name, age) " +
//            "VALUES (?,?,?)";
//
//
//    //language=SQL
//    private static final String SQL_SELECT = "select * from student";
//
//    //language=SQL
//    private static final String SQL_SElECT_BY_ID= "select * from student";
//
//    //language=SQL
//    private static final String SQL_SELECT_BY_FIRST_AND_LAST_NAME=
//            "select * from student where first_name=? and last_name=?";
//
//    //language=SQL
//    private static final String SQL_DELETE_BY_ID=
//            "delete from student where id=?";
//
//    private DataSource dataSource;
//    private SimpleJdbcTemplate<User> simpleJdbcTemplate;
//
//    public UsersRepositoryJdbcImpl(DataSource dataSource) {
//        this.dataSource = dataSource;
//        simpleJdbcTemplate=new SimpleJdbcTemplate<>(dataSource);
//    }
//
//
//    private RowMapper<User> userRowMapper = row -> User.builder()
//            .id(row.getLong("id"))
//            .firstName(row.getString("first_name"))
//            .lastName(row.getString("last_name"))
//            .age(row.getInt("age"))
//            .build();
//
//
//
//
//    @Override
//    public List<User> findAllByAge(Integer age) {
//        return  simpleJdbcTemplate.query(SQL_SELECT_BY_AGE, userRowMapper, age);
//
//    }
//    @Override
//    public List<User> findAll() {
//
////        Connection connection = null;
////        PreparedStatement statement = null;
////        ResultSet resultSet = null;
////
////        try {
////            connection = dataSource.getConnection();
////            statement = connection.prepareStatement(SQL_SELECT);
////            resultSet = statement.executeQuery();
////
////            List<User> users = new ArrayList<>();
////
////            while (resultSet.next()) {
////                User user = userRowMapper.mapRow(resultSet);
////                users.add(user);
////            }
////            return users;
////
////        } catch (SQLException e) {
////            throw new IllegalStateException(e);
////        } finally {
////            if (resultSet != null) {
////                try {
////                    resultSet.close();
////                } catch (SQLException ignore) {}
////            }
////            if (statement != null) {
////                try {
////                    statement.close();
////                } catch (SQLException ignore) {}
////            }
////            if (connection != null) {
////                try {
////                    connection.close();
////                } catch (SQLException ignore) {}
////            }
////        }
//        return simpleJdbcTemplate.query(SQL_SELECT, userRowMapper);
//    }
//
//    @Override
//    public Optional<User> findFirstByFirstnameAndLastname(String firstName, String lastName) {
//        return null;
//    }
//
//    @Override
//    public Optional<User> findById(Long id) {
//        return null;
//    }
//
//    @Override
//    public void deleteById(Long id) {
//
//    }
//
//
//    @Override
//    public void save(User entity) {
//
//    }
//
//    @Override
//    public void update(User entity) {
//
//    }
//
//
//    @Override
//    public void delete(User entity) {
//
//    }
//}
