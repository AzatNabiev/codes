package ru.itis.javalab.repositories;

import ru.itis.javalab.models.User;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryImpl implements UsersRepository {

    public static final String SQL_FIND_BY_EMAIL_AND_PASSWORD = "SELECT * FROM  USERS WHERE email=? AND password=?";
    public static final String SQL_FIND_ALL_BY_AGE="SELECT * FROM USERS WHERE age=?";
    public static final String SQL_FIND_ALL="SELECT * FROM USERS";
    public static final String SQL_SAVE_USER="INSERT INTO USERS (first_name, last_name, password, email) VALUES (?, ?, ?, ?);";

    private SimpleJdbcTemplate template;

    private final RowMapper<User> userRowMapper = row -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getInt("age"))
            .build();

    public UsersRepositoryImpl(DataSource dataSource) {
        template = new SimpleJdbcTemplate(dataSource);
    }

    @Override
    public List<User> findAllByAge(Integer age) {
        List<User> user=template.query(SQL_FIND_ALL_BY_AGE,userRowMapper,age);
        return user.isEmpty()? null:user;
    }


    @Override
    public List<User> findAll() {
        List<User> user=template.query(SQL_FIND_ALL,userRowMapper);
        return user.isEmpty()?null:user;
    }

    @Override
    public Optional<User> findFirstByEmailAndPassword(String email, String pass) {
        List<User> user=template.query(SQL_FIND_BY_EMAIL_AND_PASSWORD, userRowMapper, email, pass);
        return user.isEmpty()?Optional.empty():Optional.of(user.get(0));
    }

    @Override
    public void save(User entity) {
        template.checkQuery(SQL_SAVE_USER, entity.getFirstName(),entity.getLastName(),entity.getEmail(),entity.getPassword());
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

    }

}
