package ru.itis.javalab.servlets;

import ru.itis.javalab.models.User;
import ru.itis.javalab.services.CookiesService;
import ru.itis.javalab.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@WebServlet("/login")
public class AuthServlet extends HttpServlet {
    private CookiesService cookiesService;
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) {
        ServletContext context = config.getServletContext();
        cookiesService = (CookiesService) context.getAttribute("CookiesService");
        usersService = (UsersService) context.getAttribute("UsersService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("/public/login.html").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("login").trim();
        String pass = req.getParameter("password").trim();
        Optional<User> user = usersService.findUserByEmailAndPass(email, pass);
        if (user.isPresent()) {
            Long userId = user.get().getId();
            UUID uuid = UUID.randomUUID();
            cookiesService.addCookieToDbByUserId(userId, uuid.toString());
            Cookie cookie = new Cookie("AUTH", uuid.toString());
            resp.addCookie(cookie);
            resp.sendRedirect("/profile");

        } else {
            resp.sendRedirect("/signup");
        }
    }
}
