package ru.itis.javalab.servlets;

import ru.itis.javalab.models.User;
import ru.itis.javalab.services.CookiesService;
import ru.itis.javalab.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class SignUpServlet extends HttpServlet {
    private CookiesService cookiesService;
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context=config.getServletContext();
        cookiesService=(CookiesService)context.getAttribute("cookiesService");
        usersService=(UsersService)context.getAttribute("usersService");
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/public/signup.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String,String> map = new HashMap<>();
        String password = req.getParameter("password");
        String password_repeat = req.getParameter("password_repeat");
        if (password.equals(password_repeat)) {
            map.put("firstname", req.getParameter("firstName"));
            map.put("lastname", req.getParameter("lastName"));
            map.put("email", req.getParameter("email"));
            map.put("password", password);
            usersService.saveUser(map);
            resp.sendRedirect("public/login.html");
        } else {
            resp.sendRedirect("public/signup.html");
        }



    }
}
