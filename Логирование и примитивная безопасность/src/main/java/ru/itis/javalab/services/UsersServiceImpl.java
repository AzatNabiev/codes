package ru.itis.javalab.services;

import ru.itis.javalab.models.User;
import ru.itis.javalab.repositories.UsersRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository){
        this.usersRepository=usersRepository;
    }
    @Override
    public Optional<User> findUserByEmailAndPass(String email, String pass) {
        return usersRepository.findFirstByEmailAndPassword(email,pass);
    }

    @Override
    public void saveUser(Map pool) {
        User user = User.builder()
                .firstName((String) pool.get("firstname"))
                .lastName((String) pool.get("lastname"))
                .password((String) pool.get("password"))
                .email((String) pool.get("email"))
                .build();
        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }
}
