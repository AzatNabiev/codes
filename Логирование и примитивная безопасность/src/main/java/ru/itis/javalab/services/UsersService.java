package ru.itis.javalab.services;

import ru.itis.javalab.models.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UsersService {
    Optional<User> findUserByEmailAndPass(String email, String pass);
    void saveUser(Map pool);
    List<User> getAllUsers();
}
