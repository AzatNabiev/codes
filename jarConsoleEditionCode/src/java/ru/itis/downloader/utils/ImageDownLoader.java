package ru.itis.downloader.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class ImageDownLoader {

    private URL url;
    private URLConnection conn;
    private Random random=new Random();
    private String name;
    public void download(String link, String pathToSave){
        try{
            url = new URL(link); //Формирование url-адреса файла
            conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);

            name=String.valueOf(random.nextInt(100));

            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
            FileOutputStream fos = new FileOutputStream(new File(pathToSave+"//"+name+".jpg"));

            int ch;
            while ((ch = bis.read())!=-1)
            {
                fos.write(ch);
            }
            bis.close();
            fos.flush();
            fos.close();

        } catch (MalformedURLException e) {e.printStackTrace();}
        catch (IOException e) {e.printStackTrace();}
    }

}
