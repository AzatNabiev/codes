package ru.itis.downloader.utils;

import com.beust.jcommander.IStringConverter;

public class UrlConverter implements IStringConverter<String[]> {
    @Override
    public String[] convert(String s) {
        return s.split(";");
    }
}