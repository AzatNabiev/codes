package ru.itis.javalab.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.javalab.repositories.UsersRepository;

public class BCrypterServiceImpl implements BCrypterService {
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Override
    public boolean checkPass(String authPass, String dbPass) {

        return passwordEncoder.matches(authPass,dbPass);
    }
}
