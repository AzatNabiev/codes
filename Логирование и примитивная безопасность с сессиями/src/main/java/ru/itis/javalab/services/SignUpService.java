package ru.itis.javalab.services;

import ru.itis.javalab.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);

}
